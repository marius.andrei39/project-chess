/* Variables and constants */

const mainGameCont = document.getElementById("gameContainer") // The container that hold the game
const infoColumn = document.getElementById("infoCol");  //The options on the right side of the game
const instructionsColumn = document.getElementById("instrCol") // the left hand pannel
const gameColumn = document.getElementById("gameCol");  //The Game board 

const Smile = document.getElementById("chesshireSmile"); //the image that appears and dissapears in the corners of the screen

const mainMenuSec = document.getElementById("mainMenuCont"); // The main menu
const accordionMenu = document.getElementById("accordion"); // The accordion
const nameInputField = document.getElementById("nameInputField"); // the Name input field

const playGameBtn = document.getElementById("playGame"); // Play btn
const settingsBtn = document.getElementById("settingsBtn"); //settings btn
const compVsCompButton = document.getElementById("compVsCompMenuBtn"); // Computer vs Computer button
const backToMenuButton = document.getElementById("backToMenu"); //Back to menu button
const infoButton = document.getElementById("infoButton"); // the button of the info panel on the right hand side of the screen
const instructionsButton = document.getElementById("instrButton"); // the button of the instructions panel on the right hand side of the screen
const infoText = document.getElementsByClassName("instr-column-text")[0].querySelectorAll("p"); // the p elements in the insctruction panel

const accordionBtns = document.getElementsByClassName("inGameOpts"); // Menu buttons

let title = document.getElementsByClassName("title")[0]; // the title 

let playersName;    // variable that holdsthe name of the player
let playerPlaying = true; // boolean that shows when there is a human player  (not comp vs comp)

let index = 1;   //a counter
let posIndex = generateRandomNumber(1, 4); // the randomly generated position of the Smile

/* -------------------------- */

/* The Event listeners for the buttons and for the O key*/

// window.addEventListener('keydown', onKeyPressEvent);

playGameBtn.addEventListener("click", onPlayButtonClick);

settingsBtn.addEventListener("click", onSettingsButtonClick);

backToMenuButton.addEventListener("click", onBackToMenuButtonClick);

compVsCompButton.addEventListener("click", onCompVsCompPress);

infoButton.addEventListener("click", onInfoButtonClick);

instructionsButton.addEventListener("click", onInstructionsButtonClick);

/* -------------------------------- */


/* Event listener functions */
let isInvis = false;    

function onCompVsCompPress() {  // this makes the compVsComp button in the settings menu not clickable, for .5 of a second after the button is clicked
    if (isInvis == false) {     // it's a bug fix
        console.log("in if");
        playGameBtn.style.animation = "hideBtn .5s ease-in";
        playGameBtn.style.opacity = 0;
        playGameBtn.style.pointerEvents = "none";
        isInvis = true;
        setTimeout(() => {
            playGameBtn.style.animation = "none";
        }, 501);
    } else {
        console.log("in else");
        playGameBtn.style.animation = "hideBtn reverse .5s ease-in";
        playGameBtn.style.opacity = 1;
        playGameBtn.style.pointerEvents = "auto";
        isInvis = false;
        setTimeout(() => {
            playGameBtn.style.animation = "none";
        }, 501);
    }
}

function onPlayButtonClick() {  // Whant happens when the play button is clicked
    boardBackground();
    movingSmile();
    changeTextColor();

    playerPlaying = true;

    mainGameCont.classList.remove("hide");

    mainMenuSec.classList.add("hide");
}

function onSettingsButtonClick() {  //What happens when the settings button is clicked
    settingsBackground();
    title.innerText = "Settings"
    accordionMenu.classList.remove("hidden");
    settingsBtn.classList.add("hidden");
    nameInputField.classList.remove("hidden")
}

function onBackToMenuButtonClick() {    //What happens when the back to menu button is clicked
    window.location.reload();
}

function onInfoButtonClick() {      // What happens when the info button is clicked
    if (infoColumn.classList.contains("out")) {
        infoColumn.classList.remove("out");
        infoColumn.style.transition = "all 1s"
        infoColumn.style.transform = "translate(100%, 10%)";

    } else {
        infoColumn.classList.add("out");
        infoColumn.style.transition = "all 1s"
        infoColumn.style.transform = "translate(2%, 10%)";
    }
}

function onInstructionsButtonClick() {  //what happens when the instructions button is clicked
    if (instructionsColumn.classList.contains("out")) {
        instructionsColumn.classList.remove("out");
        instructionsColumn.style.transition = "all 1s"
        instructionsColumn.style.transform = "translate(-100%, 10%)";

    } else {
        instructionsColumn.classList.add("out");
        instructionsColumn.style.transition = "all 1s"
        instructionsColumn.style.transform = "translate(0%, 10%)";
    }

}

// function onKeyPressEvent(event) {   // What happens when the o key is pressed
//     if (event.key == "o") {
//         if (!mainGameCont.classList.contains("hide")) {
//             mainGameCont.classList.add("hide");
//         } else {
//             mainGameCont.classList.remove("hide");
//         }

//         if (mainMenuSec.classList.contains("hide")) {
//             mainMenuSec.classList.remove("hide");
//         } else {
//             mainMenuSec.classList.add("hide");
//         }
//     }
// }
/* ------------------ */

/* The functions */

function movingSmile() { // This one adds the animation on the image, making it fade in and out
    if (index == 1) {
        Smile.style.animation = "smileShow 15s infinite ease-in-out";
    }

    setTimeout(function () {
        let smileX;
        let smileY;

        let windowWidth = window.innerWidth;
        let windowHeight = window.innerHeight;

        console.log(`${posIndex} ${index}`)


        if (windowWidth > 768) {
            switch (posIndex) {
                case 1:
                    smileX = windowWidth * 80 / 100;
                    smileY = 0;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "0deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 2:
                    smileX = windowWidth * 80 / 100;
                    smileY = windowHeight * 80 / 100;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "0deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 3:
                    smileX = 0
                    smileY = windowHeight * 80 / 100;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "-60deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 4:
                    smileX = 0;
                    smileY = 0;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "-60deg";
                    posIndex = generateRandomNumber(1, 4);
                    break
            }

        } else {
            switch (posIndex) {
                case 1:
                    smileX = windowWidth * 80 / 100;
                    smileY = 0;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "0deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 2:
                    smileX = windowWidth * 80 / 100;
                    smileY = windowHeight * 85 / 100;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "0deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 3:
                    smileX = 0
                    smileY = windowHeight * 85 / 100;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "-60deg";
                    posIndex = generateRandomNumber(1, 4);
                    break;

                case 4:
                    smileX = 0;
                    smileY = 0;
                    Smile.style.translate = `${smileX}px ${smileY}px`;
                    Smile.style.rotate = "-60deg";
                    posIndex = generateRandomNumber(1, 4);
                    break
            }

        }
        if (index <= 100) {
            movingSmile();
        }

        index++;

    }, 15000)   //every 15 seconds
}

function changeTextColor() {        //This changes the color of the text
    infoText.forEach(element=>{
        element.style.color = "var(--primary-menu-btn-color)";
    })
}

function generateRandomNumber(min, max) {   // a function to generate a random number
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/* ---------------------- */

/* The end of this js file */