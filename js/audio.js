/* This file takes care of the audio of the game */


const turnAudio = document.querySelector(".stopAudio");
let audioVar = document.getElementById("backgroundMusic");

turnAudio.addEventListener("click", backgroundMusic);

function backgroundMusic() {
    if(audioVar.currentTime != 0){
        audioVar.pause();
        audioVar.currentTime = 0;
        turnAudio.style.backgroundImage = "url('img/misc/mute.png')";
    }else{
        audioVar.play();
        turnAudio.style.backgroundImage = "url('img/misc/audio-speaker.png')";
    }
}