
/* These are for changeing the background when moving through the game*/

//Changing background on click


function settingsBackground(){
  document.body.style.backgroundImage = "url('img/backgrounds/settings.jpg')";
}

function boardBackground(){
  if(document.body.clientWidth < 425){
    document.body.style.backgroundImage = "url('img/backgrounds/phoneBoard.jpeg')";
  }else{
    document.body.style.backgroundImage = "url('img/backgrounds/game.jpg')";
  }
}